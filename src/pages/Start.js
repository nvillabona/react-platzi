import React from 'react';
import { Link } from 'react-router-dom';

import './styles/Start.css';




function Start() {


    return (
        <div className="start-container">
            <div className="card">
                <div className="card-body justify-content-center">
                    <h1>Hello</h1>
                    <Link className="btn btn-primary" to="/badges">Enter</Link>
                </div>

            </div>
        </div>



    )


}


export default Start;