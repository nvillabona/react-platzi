import React from 'react';

import DeleteBadgeModal from '../components/DeleteBadgeModal';
import Logo from '../images/platziconf-logo.svg';
import Badge from '../components/Badge';
import './styles/BadgeDetails.css'
import {Link,} from 'react-router-dom';

function BadgeDetails (props){

    const badge = props.badge;

    return(

        <div>

            <div className="BadgeDetails__hero">
                <div className="container">
                    <div className="row">
                        <div className="col-6">
                            <img src={Logo} alt="Logo de la conferencia" />
                        </div>
                        <div className="col-6 BadgeDetails__hero-attendant-name">
                            
                            {badge.firstName}

                        </div>
                    </div>
                </div>
                
            </div>
            <div className="container">
                    <div className="row">
                        <div className="col-8">
                            <Badge 
                            firstName={badge.firstName} 
                            lastName={badge.lastName}
                            email={badge.email}
                            twitter={badge.twitter}
                            jobTitle={badge.jobTitle}
                             />
                        </div>
                        <div className="col-4">
                            <h2>Actions</h2>
                            <div>
                                <Link to={`/badges/${badge.id}/edit`} className="btn btn-primary">Edit</Link>
                            </div>
                            <div>
                                <button  onClick={props.onOpenModal} className="mt-2 btn btn-danger">Delete</button>
                                
                                <DeleteBadgeModal  
                                    onClose={props.onCloseModal} 
                                    isOpen={props.modalIsOpen}
                                    onDeleteBadge={props.onDeleteBadge}>
                                    
                                </DeleteBadgeModal>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )
}

export default BadgeDetails;