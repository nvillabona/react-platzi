import React from 'react';
import { Link } from 'react-router-dom';
import './styles/BadgesList.css';

class BadgesList extends React.Component {

    render() {
        if(this.props.badge.length === 0){
            return(
                <div>
                    <h3>No encontramos ningun badge</h3>
                    <Link className='btn btn-primary' to='badges/new' />
                </div>
            )
        }
        return (
            <ul className="list-unstyled">
                {this.props.badge.map((badge) => {
                    return (
                        <div className="BadgesListItem" key={badge.id}>
                            <Link className="text-reset text-decoration-none grid" to={`/badges/${badge.id}`}>
                                <img
                                    className="BadgesListItem__avatar"
                                    src={badge.avatarUrl}
                                    alt={`${badge.firstName} ${badge.lastName}`}
                                />

                                <div>
                                    <strong>
                                        {badge.firstName} {badge.lastName}
                                    </strong>
                                    <br />@{badge.twitter}
                                    <br />
                                    {badge.jobTitle}
                                </div>
                            </Link>
                        </div>

                    )
                })}
            </ul>
        )
    }
}

export default BadgesList;